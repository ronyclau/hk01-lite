import "./App.css";

import { Route, Routes } from "solid-app-router";
import {
  Component,
  ErrorBoundary,
  Suspense,
  createResource,
  lazy,
} from "solid-js";
import { Portal } from "solid-js/web";

import Menu from "./components/Menu";
import PageLoadingIndicator from "./components/PageLoadingIndicator";
import SettingsBtn from "./components/SettingsBtn";
import { SettingsProvider } from "./contexts/SettingsContext";
import ArticlePageData from "./pages/ArticlePage/data";
import CategoryPageData from "./pages/CategoryPage/data";
import HomePageData from "./pages/HomePage/data";
import HotPageData from "./pages/HotPage/data";
import IssuePageData from "./pages/IssuePage/data";
import LatestPageData from "./pages/LatestPage/data";
import RedirectPage from "./pages/RedirectPage";
import TagPageData from "./pages/TagPage/data";
import ZonePageData from "./pages/ZonePage/data";
import { fetchHomeMenu } from "./utils/fetchers";

const ArticlePage = lazy(() => import("./pages/ArticlePage"));
const CategoryPage = lazy(() => import("./pages/CategoryPage"));
const ErrorPage = lazy(() => import("./pages/ErrorPage"));
const HomePage = lazy(() => import("./pages/HomePage"));
const HotPage = lazy(() => import("./pages/HotPage"));
const IssuePage = lazy(() => import("./pages/IssuePage"));
const LatestPage = lazy(() => import("./pages/LatestPage"));
const TagPage = lazy(() => import("./pages/TagPage"));
const ZonePage = lazy(() => import("./pages/ZonePage"));

const App: Component = () => {
  let loadingIndicatorWrapper: HTMLDivElement | undefined;
  const [menuData] = createResource(fetchHomeMenu);

  return (
    <ErrorBoundary fallback={(err) => <ErrorPage error={err} />}>
      <SettingsProvider>
        <div className="lg:max-w-screen-md xl:max-w-screen-lg 2xl:max-w-screen-xl mx-auto py-4 lg:py-8 relative">
          <div id="loading-indicator" ref={loadingIndicatorWrapper}></div>
          <Suspense fallback={<nav />}>
            <Menu
              menu={menuData() ?? []}
              withHome
              className="text-lg mb-4 mx-4 lg:mx-0"
            />
          </Suspense>
          <Suspense>
            <Routes>
              <Portal mount={loadingIndicatorWrapper}>
                <PageLoadingIndicator />
              </Portal>
              <Route path="/" element={<HomePage />} data={HomePageData} />
              <Route
                path="/article/:id"
                element={<ArticlePage />}
                data={ArticlePageData}
              />
              <Route
                path="/category/:id"
                element={<CategoryPage />}
                data={CategoryPageData}
              />
              <Route
                path="/zone/:id"
                element={<ZonePage />}
                data={ZonePageData}
              />
              <Route
                path="/issue/:id"
                element={<IssuePage />}
                data={IssuePageData}
              />
              <Route path="/tag/:id" element={<TagPage />} data={TagPageData} />
              <Route path="/hot" element={<HotPage />} data={HotPageData} />
              <Route
                path="/latest"
                element={<LatestPage />}
                data={LatestPageData}
              />
              <Route path="/*all" element={<RedirectPage />} />
            </Routes>
          </Suspense>
        </div>
        <SettingsBtn />
      </SettingsProvider>
    </ErrorBoundary>
  );
};

export default App;
