import classNames from "classnames";
import { Link } from "solid-app-router";
import {
  Component,
  For,
  Match,
  Show,
  Suspense,
  Switch,
  createResource,
} from "solid-js";

import type { PageListing, PageSection } from "@src/types/Pages";
import { genericJsonFetcher, withOffset } from "@src/utils/fetchers";
import { rewriteHK01Url } from "@src/utils/url";

import FeedItemListing from "./FeedItemListing";
import LazyLoadingListing from "./LazyLoadingListing";

export interface Props {
  className?: string;
  sections: PageSection[];
}

const nextPageFetcher = (
  url: string,
  nextOffset?: number | string
): Promise<PageListing> =>
  genericJsonFetcher(withOffset(url.replace(/^\//, ""), nextOffset));

const Section: Component<{ section: PageSection }> = (props) => {
  const [data] = createResource(props.section, (section) => {
    if (section.nextPageUrl && section.items.length === 0) {
      return nextPageFetcher(section.nextPageUrl, section.nextOffset);
    } else {
      return Promise.resolve(props.section);
    }
  });

  return (
    <section className="flex flex-col gap-4">
      <div className="flex items-center justify-between gap-4">
        <h2 className="text-xl font-bold empty:hidden">{props.section.name}</h2>
        <Show when={props.section.moreUrl}>
          <Link
            href={rewriteHK01Url(props.section.moreUrl ?? "/")}
            referrerpolicy="same-origin"
          >
            More
          </Link>
        </Show>
      </div>
      <Suspense>
        <Switch fallback={<FeedItemListing items={data()?.items ?? []} />}>
          <Match
            when={props.section.nextPageUrl && props.section.items.length === 0}
          >
            <LazyLoadingListing
              data={data()}
              fetcher={(nextOffset?: number | string) =>
                nextPageFetcher(props.section.nextPageUrl ?? "", nextOffset)
              }
            />
          </Match>
        </Switch>
      </Suspense>
    </section>
  );
};

const Sections: Component<Props> = (props) => (
  <div className={classNames("flex flex-col gap-4", props.className)}>
    <For each={props.sections}>
      {(section, i) => (
        <>
          <Show when={i()}>
            <hr />
          </Show>
          <Section section={section} />
        </>
      )}
    </For>
  </div>
);

export default Sections;
