import { Link } from "solid-app-router";
import type { Component } from "solid-js";

import type { TagInfo } from "@src/types/Tag";

export interface Props {
  tag: TagInfo;
}

export const Tag: Component<Props> = (props) => (
  <Link
    className="px-2 text-base leading-8 bg-blue-100"
    href={`/tag/${props.tag.tagId}`}
  >
    {props.tag.tagName}
  </Link>
);
