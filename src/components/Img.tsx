import classNames from "classnames";
import { Component, JSX, splitProps } from "solid-js";

export type Props = JSX.ImgHTMLAttributes<HTMLImageElement>;

const Img: Component<Props> = (props) => {
  const [local, restProps] = splitProps(props, ["className"]);
  return (
    <img
      loading="lazy"
      referrerpolicy="same-origin"
      className={classNames("bg-gray-100", local.className)}
      {...restProps}
    />
  );
};

export default Img;
