import classNames from "classnames";
import { Component, JSX, createMemo, splitProps } from "solid-js";

import type { Media } from "@src/types/Article";

import { DEFAULT_GALLERY_ID } from "./ArticleBlock/GalleryBlock";
import Img from "./Img";

export interface Props extends JSX.ImgHTMLAttributes<HTMLImageElement> {
  linkClass?: string;
  linkHref?: string;
  galleryId?: string;
  media: Media;
  ratio?: "16_9" | "4_3" | "1";
  noLink?: boolean;
}

const availableSizes = [320, 480, 640, 800, 960, 1120, 1280];

const MediaImg: Component<Props> = (props) => {
  const [local, restProps] = splitProps(props, [
    "linkClass",
    "linkHref",
    "galleryId",
    "media",
    "ratio",
    "className",
    "noLink",
  ]);
  const ratioSuffix = local.ratio ? `r${local.ratio}` : "";
  const srcset = createMemo(() =>
    availableSizes
      .map(
        (size, i) =>
          `${local.media.cdnUrl}?v=w${size}${ratioSuffix}` +
          (i === availableSizes.length - 1 ? "" : ` ${size}w`)
      )
      .join(", ")
  );

  return local.noLink ? (
    <Img
      className={classNames(
        local.className ?? "object-cover object-center",
        local.ratio === "16_9"
          ? "aspect-video"
          : local.ratio === "4_3"
          ? "aspect-[4/3]"
          : local.ratio === "1"
          ? "aspect-square"
          : ""
      )}
      src={`${local.media.cdnUrl}?v=w480${ratioSuffix}`}
      srcset={srcset()}
      {...restProps}
    />
  ) : (
    <a
      className={classNames(
        local.linkClass ?? "flex flex-col",
        local.ratio === "16_9"
          ? "aspect-video"
          : local.ratio === "4_3"
          ? "aspect-[4/3]"
          : local.ratio === "1"
          ? "aspect-square"
          : ""
      )}
      href={local.linkHref ?? `${local.media.cdnUrl}?v=w1920`}
      target="_blank"
      rel="noreferrer noopener"
      referrerpolicy="same-origin"
      data-gallery-id={local.galleryId ?? DEFAULT_GALLERY_ID}
      data-caption={local.media.caption}
    >
      <Img
        className={local.className ?? "object-cover object-center h-full"}
        src={`${local.media.cdnUrl}?v=w480${ratioSuffix}`}
        srcset={srcset()}
        {...restProps}
      />
    </a>
  );
};

export default MediaImg;
