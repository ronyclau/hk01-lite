import classNames from "classnames";
import { Link } from "solid-app-router";
import { Component, Show } from "solid-js";

import type { ListArticle } from "@src/types/Article";
import type { ListIssue } from "@src/types/Issue";
import { formatDate, relativeDate } from "@src/utils/date";

import MediaImg from "./MediaImg";

export interface Props {
  data: ListArticle | ListIssue;
  className?: string;
}

const formatVideoDuration = (duration: number) =>
  Math.floor(duration / 60)
    .toString()
    .padStart(2, "0") +
  ":" +
  (duration % 60).toString().padStart(2, "0");

const ArticleIssueCard: Component<Props> = (props) => (
  <Link
    className={classNames("flex flex-col", props.className)}
    href={
      "articleId" in props.data
        ? `/article/${props.data.articleId}`
        : `/issue/${props.data.issueId}`
    }
    title={props.data.title}
  >
    <div className="relative flex flex-col">
      <MediaImg ratio="16_9" media={props.data.mainImage} noLink />
      <Show when={"video" in props.data && props.data.video}>
        <div className="absolute right-0 bottom-0 flex gap-1 items-center bg-black bg-opacity-50 text-sm text-white px-2 py-px">
          <span>🎦</span>
          <span>
            {formatVideoDuration(
              (props.data as ListArticle).video?.duration ?? 0
            )}
          </span>
        </div>
      </Show>
    </div>
    <div className="mb-1 text-lg h-14 overflow-hidden">{props.data.title}</div>
    <div className="flex items-center justify-between text-gray-500 text-sm">
      <div className="flex gap-1 items-center">
        <span>
          {"articleId" in props.data
            ? props.data.mainCategory
            : props.data.zonePublishName}
        </span>
        <Show when={props.data.isSponsored}>
          <span className="bg-yellow-100 text-center inline-block w-5">📌</span>
        </Show>
      </div>
      <div className="flex gap-1 items-center">
        <Show when={props.data.isSponsored}>
          <span
            className="bg-black text-center inline-block w-5"
            title="Sponsored"
          >
            💰
          </span>
        </Show>
        <time
          datetime={new Date(props.data.publishTime * 1000).toISOString()}
          title={formatDate(props.data.publishTime * 1000, "PPPPpppp")}
        >
          {relativeDate(props.data.publishTime * 1000, new Date())}
        </time>
      </div>
    </div>
  </Link>
);

export default ArticleIssueCard;
