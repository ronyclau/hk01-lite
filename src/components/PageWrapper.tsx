import classNames from "classnames";
import type { Component } from "solid-js";

export interface Props {
  className?: string;
}

const PageWrapper: Component<Props> = (props) => (
  <div
    className={classNames(
      "flex flex-col gap-4 px-4 lg:px-0 lg:text-lg",
      props.className
    )}
  >
    {props.children}
  </div>
);

export default PageWrapper;
