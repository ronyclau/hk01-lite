import classNames from "classnames";
import { useIsRouting } from "solid-app-router";
import { Component, Show, createEffect, createSignal, on } from "solid-js";

const PageLoadingIndicator: Component = () => {
  const isRouting = useIsRouting();
  const [hide, setHide] = createSignal(!isRouting());

  createEffect(
    on(isRouting, () => {
      if (isRouting()) {
        setHide(false);
      }
    })
  );

  return (
    <Show when={!hide()}>
      <div
        className={classNames(
          "fixed top-0 left-0 right-0 w-full z-50 h-1 bg-blue-500",
          isRouting()
            ? "animate-[slide-right-offseted_ease-out_3s_forwards_1,pulse_2s_cubic-bezier(0.4,0,0.6,1)_infinite]"
            : "animate-[slide-right-offseted_ease-out_3s_forwards_1]"
        )}
      />
      <div
        className={classNames(
          "fixed top-0 left-0 right-0 w-full z-[49] h-1 bg-blue-500",
          isRouting()
            ? "hidden"
            : "animate-[slide-right_linear_0.5s_forwards_1]"
        )}
        onanimationend={[setHide, true]}
      />
    </Show>
  );
};

export default PageLoadingIndicator;
