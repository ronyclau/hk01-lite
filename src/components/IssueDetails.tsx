import classNames from "classnames";
import type { Component } from "solid-js";

import type { Issue } from "@src/types/Issue";

import ArticleIssueDetails from "./ArticleIssueDetails";

export interface Props {
  className?: string;
  issue: Issue;
}

const IssueDetails: Component<Props> = (props) => (
  <div
    className={classNames("w-full empty:hidden text-justify", props.className)}
  >
    <ArticleIssueDetails data={props.issue} />
  </div>
);

export default IssueDetails;
