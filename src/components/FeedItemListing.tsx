import classNames from "classnames";
import { Component, For, Match, Show, Switch, useContext } from "solid-js";

import { SettingsContext } from "@src/contexts/SettingsContext";
import type { ListArticle } from "@src/types/Article";
import type { FeedItem } from "@src/types/FeedItem";
import type { ListIssue } from "@src/types/Issue";

import ArticleIssueCard from "./ArticleIssueCard";

export interface Props {
  className?: string;
  items: FeedItem[];
}

const FeedItemListing: Component<Props> = (props) => {
  const [state] = useContext(SettingsContext);

  return (
    <div
      className={classNames(
        "flex flex-col gap-4 lg:grid lg:grid-cols-2 xl:grid-cols-3",
        props.className
      )}
    >
      <For each={props.items}>
        {(item) => (
          <Switch>
            <Match when={item.type === 1}>
              <Show
                when={
                  !(item.data as ListArticle).isSponsored ||
                  !state.hideSponsored
                }
              >
                <ArticleIssueCard data={item.data as ListArticle} />
              </Show>
            </Match>
            <Match when={item.type === 2}>
              <ArticleIssueCard data={item.data as ListIssue} />
            </Match>
          </Switch>
        )}
      </For>
    </div>
  );
};

export default FeedItemListing;
