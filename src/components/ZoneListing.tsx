import type { Component } from "solid-js";

import Menu from "@src/components/Menu";
import type { PageZone } from "@src/types/Pages";

import Sections from "./Sections";

export interface Props {
  pageZone: PageZone;
}

const ZoneListing: Component<Props> = (props) => (
  <>
    <Menu menu={props.pageZone.menu} />
    <h1 className="text-3xl font-bold">{props.pageZone.zone.publishName}</h1>
    <Sections sections={props.pageZone.sections} />
  </>
);

export default ZoneListing;
