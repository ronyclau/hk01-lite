import classNames from "classnames";
import { Component, For } from "solid-js";

import type { Article } from "@src/types/Article";

import ArticleIssueDetails from "./ArticleIssueDetails";
import RelatedArticles from "./RelatedArticles";
import { Tag } from "./Tag";

export interface Props {
  className?: string;
  article: Article;
}

const ArticleDetails: Component<Props> = (props) => {
  return (
    <div
      className={classNames(
        "w-full empty:hidden text-justify flex flex-col gap-4",
        props.className
      )}
    >
      <ArticleIssueDetails data={props.article} />
      <div className="flex flex-wrap gap-2">
        <For each={props.article.tags}>{(tag) => <Tag tag={tag} />}</For>
      </div>
      <hr />
      <RelatedArticles articleId={props.article.articleId} />
    </div>
  );
};

export default ArticleDetails;
