import classNames from "classnames";
import { Component, Show, createEffect, createSignal } from "solid-js";

import type { Feed } from "@src/types/Feed";

import FeedItemListing from "./FeedItemListing";

export interface Props {
  className?: string;
  data?: Feed;
  fetcher: (nextOffset?: number | string) => Promise<Feed>;
}

const LoadMoreBtn: Component<
  Pick<Props, "className" | "fetcher"> &
    Pick<Feed, "nextOffset"> & {
      cb: (feed: Feed) => void;
    }
> = (props) => {
  const [loading, setLoading] = createSignal(false);

  return (
    <button
      className="bg-blue-50 p-2 rounded text-blue-900 disabled:bg-gray-100 disabled:text-gray-600"
      disabled={loading()}
      onClick={() => {
        setLoading(true);
        props
          .fetcher(props.nextOffset)
          .then(props.cb)
          .finally(() => {
            setLoading(false);
          });
      }}
    >
      Load More
    </button>
  );
};

const LazyLoadingListing: Component<Props> = (props) => {
  const [feed, setFeed] = createSignal<Feed>({ items: [] });

  const nextOffset = () => feed().nextOffset;

  createEffect(() => {
    setFeed(props.data ?? { items: [] });
  });

  return (
    <div className={classNames("flex flex-col gap-4", props.className)}>
      <FeedItemListing items={feed().items} />
      <Show when={nextOffset() !== undefined}>
        <LoadMoreBtn
          fetcher={props.fetcher}
          nextOffset={nextOffset()}
          cb={(nextPage) => {
            setFeed((prevFeed) => ({
              items: prevFeed.items.concat(nextPage.items),
              nextOffset: nextPage.nextOffset,
            }));
          }}
        />
      </Show>
    </div>
  );
};

export default LazyLoadingListing;
