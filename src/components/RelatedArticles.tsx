import classNames from "classnames";
import { Component, Suspense, createResource } from "solid-js";

import { fetchRelatedArticles } from "@src/utils/fetchers";

import FeedItemListing from "./FeedItemListing";

export interface Props {
  className?: string;
  articleId: number | string;
}

const RelatedArticles: Component<Props> = (props) => {
  const [data] = createResource(() => props.articleId, fetchRelatedArticles);
  return (
    <section className={classNames("flex flex-col", props.className)}>
      <h2 className="text-2xl font-bold mb-2">Related Articles</h2>
      <Suspense>
        <FeedItemListing items={data()?.items ?? []} />
      </Suspense>
    </section>
  );
};

export default RelatedArticles;
