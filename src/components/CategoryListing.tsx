import type { Component } from "solid-js";

import type { PageCategory } from "@src/types/Pages";

import Menu from "./Menu";
import Sections from "./Sections";

export interface Props {
  pageCategory: PageCategory;
}

const CategoryListing: Component<Props> = (props) => (
  <>
    <Menu menu={props.pageCategory.menu} />
    <h1 className="text-3xl font-bold">
      {props.pageCategory.category.publishName}
    </h1>
    <Sections sections={props.pageCategory.sections} />
  </>
);

export default CategoryListing;
