import BigPicture from "bigpicture";
import { iframeResizer } from "iframe-resizer";
import {
  Component,
  For,
  JSX,
  Match,
  Show,
  Switch,
  createEffect,
  createSignal,
  on,
  onCleanup,
  useContext,
} from "solid-js";
import { Portal } from "solid-js/web";

import { SettingsContext } from "@src/contexts/SettingsContext";
import type { Article } from "@src/types/Article";
import type {
  Block,
  BlockBrightcove,
  BlockCode,
  BlockFAQ,
  BlockGallery,
  BlockImage,
  BlockMap,
  BlockQuote,
  BlockRelated,
  BlockSummary,
  BlockTable,
  BlockText,
  BlockYoutube,
} from "@src/types/ArticleBlock";
import type { Issue } from "@src/types/Issue";
import { formatDate, relativeDate } from "@src/utils/date";
import { resizeIframe, resizeIframeOnDetailsToggle } from "@src/utils/iframe";

import CodeBlock, { Props as CodeBlockProps } from "./ArticleBlock/CodeBlock";
import FAQBlock from "./ArticleBlock/FAQBlock";
import GalleryBlock from "./ArticleBlock/GalleryBlock";
import ImageBlock from "./ArticleBlock/ImageBlock";
import MapBlock from "./ArticleBlock/MapBlock";
import QuoteBlock from "./ArticleBlock/QuoteBlock";
import RelatedBlock from "./ArticleBlock/RelatedBlock";
import SelfHostedVideoBlock from "./ArticleBlock/SelfHostedVideoBlock";
import SummaryBlock from "./ArticleBlock/SummaryBlock";
import TableBlock from "./ArticleBlock/TableBlock";
import TextBlock from "./ArticleBlock/TextBlock";
import VideoBlock from "./ArticleBlock/VideoBlock";
import FeedItemListing from "./FeedItemListing";
import MediaImg from "./MediaImg";

export interface Props {
  data: Article | Issue;
}

const prefetchedUrls = new Set<string>();

const preloadAdjacentImages = (ele: HTMLElement, group: HTMLElement[]) => {
  if (group.length === 0) {
    return false;
  }

  const index = group.indexOf(ele);
  if (index === -1) {
    return false;
  }

  const urls: string[] = [];
  if (index > 0) {
    const url = group[index - 1].dataset.bp;
    if (url && !prefetchedUrls.has(url)) {
      urls.push(url);
    }
  }

  if (index < group.length - 1) {
    const url = group[index + 1].dataset.bp;
    if (url && !prefetchedUrls.has(url)) {
      urls.push(url);
    }
  }

  urls.forEach((url) => {
    prefetchedUrls.add(url);
    const img = new Image();
    img.referrerPolicy = "same-origin";
    img.src = url;
  });

  return true;
};

const ZoneCategoryInfo: Component<{
  parentUrl: string;
  parentIcon?: string | null;
  parentName: string;
  originalUrl: string;
}> = (props) => (
  <div className="-mb-2 flex items-center justify-between">
    <a
      href={props.parentUrl}
      className="flex items-start gap-1"
      referrerpolicy="same-origin"
    >
      <img
        src={props.parentIcon ?? ""}
        className="h-[1.5em] aspect-square bg-gray-100"
      />
      {props.parentName}
    </a>
    <a
      href={props.originalUrl}
      rel="noreferrer noopener"
      target="_blank"
      referrerpolicy="same-origin"
    >
      Original
    </a>
  </div>
);

const CollapsableCodeBlock: Component<
  CodeBlockProps & {
    open?: boolean;
    eager?: boolean;
    text?: string;
  }
> = (props) => {
  let detailsRef: HTMLDetailsElement | undefined;
  const eager = () => props.eager || props.open;
  const [shouldLoad, setShouldLoad] = createSignal(eager());

  const onCodeBlockToggled: JSX.EventHandler<HTMLDetailsElement, Event> = (
    ev
  ) => {
    if (eager() || shouldLoad()) {
      resizeIframeOnDetailsToggle(ev);
    } else {
      setShouldLoad(true);
    }
  };

  createEffect(() => {
    if ((props.open || shouldLoad()) && detailsRef) {
      const iframe = detailsRef.querySelector("iframe");
      iframe && iframe.offsetParent && resizeIframe(iframe);
    }
  });

  return (
    <details onToggle={onCodeBlockToggled} open={props.open} ref={detailsRef}>
      <summary>
        {(props.block as BlockCode).image ? (
          <MediaImg media={(props.block as BlockCode).image} />
        ) : (
          props.text ?? "Expand"
        )}
      </summary>
      <Show when={eager() || shouldLoad()}>
        <CodeBlock block={props.block as BlockCode} className="-mx-4 lg:mx-0" />
      </Show>
    </details>
  );
};

const Blocks: Component<Props & { blocks: Block[] }> = (props) => {
  const [settings] = useContext(SettingsContext);

  return (
    <For each={props.blocks}>
      {(block, index) => (
        <>
          <Show when={"issueId" in props.data}>
            <>
              <Show when={index() > 0}>
                <hr />
              </Show>
              <Show when={"title" in block && block.title}>
                <h3 className="font-bold">{"title" in block && block.title}</h3>
              </Show>
            </>
          </Show>
          <Switch>
            <Match
              when={
                "video" in props.data &&
                props.data.video &&
                (block.blockType === "video" ||
                  block.blockType === "brightcove") &&
                props.data.video.blockType === block.blockType &&
                props.data.video.videoId === block.videoId
              }
            >
              <></>
            </Match>
            <Match
              when={["image", "imgtxt", "txtimg"].includes(block.blockType)}
            >
              <ImageBlock
                block={block as BlockImage}
                className="-mx-4 lg:mx-0"
              />
            </Match>
            <Match when={block.blockType === "gallery"}>
              <GalleryBlock
                block={block as BlockGallery}
                className="-mx-4 lg:mx-0"
              />
            </Match>
            <Match when={block.blockType === "text"}>
              <TextBlock block={block as BlockText} />
            </Match>
            <Match when={block.blockType === "summary"}>
              <SummaryBlock block={block as BlockSummary} />
            </Match>
            <Match when={block.blockType === "quote"}>
              <QuoteBlock block={block as BlockQuote} />
            </Match>
            <Match when={block.blockType === "table"}>
              <TableBlock block={block as BlockTable} />
            </Match>
            <Match
              when={
                block.blockType === "related" || block.blockType === "featured"
              }
            >
              {"issueId" in props.data ? (
                <FeedItemListing items={(block as BlockRelated).articles} />
              ) : (
                <RelatedBlock block={block as BlockRelated} />
              )}
            </Match>
            <Match when={block.blockType === "map"}>
              <MapBlock block={block as BlockMap} className="-mx-4 lg:mx-0" />
            </Match>
            <Match when={block.blockType === "code"}>
              <CollapsableCodeBlock
                block={block as BlockCode}
                className="-mx-4 lg:mx-0"
                text="Third-party content"
                open={!settings.collapseAllCodeBlocks}
              />
            </Match>
            <Match when={block.blockType === "video"}>
              <VideoBlock
                block={block as BlockYoutube}
                className="-mx-4 lg:mx-0"
              />
            </Match>
            <Match when={block.blockType === "brightcove"}>
              <SelfHostedVideoBlock
                block={block as BlockBrightcove}
                className="-mx-4 lg:mx-0"
              />
            </Match>
            <Match when={block.blockType === "faq"}>
              <FAQBlock block={block as BlockFAQ} />
            </Match>
          </Switch>
        </>
      )}
    </For>
  );
};

const ArticleIssueDetails: Component<Props> = (props) => {
  let rootEle: HTMLDivElement | undefined;
  const now = Date.now();
  const [isGalleryOpened, setIsGalleryOpened] = createSignal(false);

  createEffect(
    on(
      () => props.data,
      () => {
        let bpRef: ReturnType<typeof BigPicture> | null = null;
        if (rootEle) {
          const eles = [
            ...(rootEle.querySelectorAll(
              "[data-gallery-id]"
            ) as NodeListOf<HTMLElement>),
          ];
          if (eles.length > 0) {
            eles.forEach((ele) => {
              if (ele.dataset.bp === undefined) {
                ele.dataset.bp =
                  ele.getAttribute("href") ?? ele.getAttribute("src") ?? "";
              }
            });
            const groups = eles.reduce<{ [key: string]: HTMLElement[] }>(
              (acc, ele) => {
                const id = ele.dataset.galleryId ?? "";
                return id in acc
                  ? { ...acc, [id]: [...acc[id], ele] }
                  : { ...acc, [id]: [ele] };
              },
              {}
            );
            Object.values(groups).forEach((group) => {
              // Hacky way to make bigpicture think this is a list of nodes.
              const arrayLikeGroups = Object.assign(
                { length: group.length },
                group
              );

              group.forEach((ele) => {
                const onChangeImage = (
                  props: [HTMLImageElement, { el: HTMLElement }]
                ) => {
                  preloadAdjacentImages(props[1].el, group);
                };
                ele.addEventListener("click", (ev) => {
                  ev.preventDefault();
                  bpRef = BigPicture({
                    el: ele,
                    gallery: arrayLikeGroups,
                    onClose: () => setIsGalleryOpened(false),
                    onChangeImage,
                  });
                  setIsGalleryOpened(true);
                  preloadAdjacentImages(ele, group);
                });
              });
            });
          }

          iframeResizer({ checkOrigin: false }, "iframe");
        }
        onCleanup(() => {
          bpRef?.close();
          bpRef = null;
          setIsGalleryOpened(false);
          console.debug("onCleanup: bp.close");
        });
      }
    )
  );

  const blocks = () => {
    let i = props.data.blocks.length - 1;
    for (; i >= 0; --i) {
      if (props.data.blocks[i].blockType !== "code") break;
    }
    ++i;

    return props.data.blocks.length === i
      ? props.data.blocks
      : props.data.blocks.slice(0, i);
  };
  const trailingCodeBlocks = () => props.data.blocks.slice(blocks().length);

  const Wrapper =
    "articleId" in props.data
      ? (props: JSX.HTMLAttributes<HTMLElement>) => <article {...props} />
      : (props: JSX.HTMLAttributes<HTMLDivElement>) => <div {...props} />;

  return (
    <Wrapper className="flex flex-col gap-4 empty:hidden" ref={rootEle}>
      <Portal mount={document.head}>
        <title>{props.data.title}</title>
        <Show when={isGalleryOpened()}>
          <style>{`html { height: 100vh; overflow: hidden; }`}</style>
        </Show>
      </Portal>
      {"articleId" in props.data ? (
        <ZoneCategoryInfo
          parentUrl={`/category/${props.data.mainCategoryId}`}
          parentIcon={props.data.mainCategoryIcon}
          parentName={props.data.mainCategory}
          originalUrl={props.data.redirectUrl ?? props.data.publishUrl}
        />
      ) : (
        <ZoneCategoryInfo
          parentUrl={`/zone/${props.data.zoneId}`}
          parentIcon={props.data.zoneIcon}
          parentName={props.data.zonePublishName}
          originalUrl={props.data.publishUrl}
        />
      )}
      <h1 className="text-xl lg:text-4xl font-bold lg:mb-4">
        <Show when={props.data.isSponsored}>
          <span
            className="bg-black text-center inline-block w-7 lg:w-10 mr-1"
            title="Sponsored"
          >
            💰
          </span>
        </Show>
        {props.data.title}
      </h1>
      <Switch
        fallback={
          <MediaImg
            media={props.data.mainImage}
            ratio="16_9"
            linkClass="-mx-4 lg:mx-0 flex flex-col"
          />
        }
      >
        <Match
          when={
            "video" in props.data &&
            props.data.video &&
            props.data.video.type === "youtube"
          }
        >
          <VideoBlock
            block={(props.data as Article).video as BlockYoutube}
            className="-mx-4 lg:mx-0 flex flex-col"
          />
        </Match>
        <Match
          when={
            "video" in props.data &&
            props.data.video &&
            props.data.video.type === "self_host_video"
          }
        >
          <SelfHostedVideoBlock
            block={(props.data as Article).video as BlockBrightcove}
            className="-mx-4 lg:mx-0 flex flex-col"
          />
        </Match>
      </Switch>
      <div className="text-gray-500 text-sm">
        <div>
          Publish time:{" "}
          <time
            datetime={new Date(props.data.publishTime * 1000).toISOString()}
            title={formatDate(
              props.data.publishTime * 1000,
              "ccc, dd LLL yyyy HH:mm:ss OOO"
            )}
          >
            {relativeDate(props.data.publishTime * 1000, now)}
          </time>
        </div>
        <Show when={props.data.publishTime !== props.data.lastModifyTime}>
          <div>
            Last update time:{" "}
            <time
              datetime={new Date(
                props.data.lastModifyTime * 1000
              ).toISOString()}
              title={formatDate(
                props.data.lastModifyTime * 1000,
                "ccc, dd LLL yyyy HH:mm:ss OOO"
              )}
            >
              {relativeDate(props.data.lastModifyTime * 1000, now)}
            </time>
          </div>
        </Show>
        <Show when={"articleId" in props.data}>
          <div>
            Authors:{" "}
            <div class="inline-flex flex-wrap gap-1">
              <For each={(props.data as Article).authors}>
                {({ publishName }) => <span>{publishName}</span>}
              </For>
            </div>
          </div>
        </Show>
      </div>
      <Show when={props.data.teaser && props.data.teaser.length > 0}>
        <SummaryBlock
          block={{ blockType: "summary", summary: props.data.teaser }}
        />
      </Show>
      <Blocks blocks={blocks()} data={props.data} />
      <For each={trailingCodeBlocks()}>
        {(block) => (
          <CollapsableCodeBlock
            block={block as BlockCode}
            className="-mx-4 lg:mx-0"
          />
        )}
      </For>
    </Wrapper>
  );
};

export default ArticleIssueDetails;
