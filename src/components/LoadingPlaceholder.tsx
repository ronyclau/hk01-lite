import classNames from "classnames";
import type { Component } from "solid-js";

export interface Props {
  className?: string;
}

const LoadingPlaceholder: Component<Props> = (props) => (
  <div
    className={classNames(
      "flex flex-col items-center justify-center gap-2",
      props.className
    )}
  >
    <div className="animate-spin bg-gray-300 relative w-10 h-10 rounded-full before:block before:w-1/2 before:h-1/2 before:bg-white before:absolute before:left-1/2 before:top-1/2 after:block after:bg-white after:absolute after:inset-[25%] after:rounded-full"></div>
  </div>
);

export default LoadingPlaceholder;
