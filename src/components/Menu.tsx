import classNames from "classnames";
import { NavLink } from "solid-app-router";
import { Component, For, Show } from "solid-js";

import type { Menu as MenuType } from "@src/types/Menu";
import { rewriteHK01Url } from "@src/utils/url";

export interface Props {
  className?: string;
  menu: MenuType;
  withHome?: boolean;
}

const Menu: Component<Props> = (props) => (
  <nav className={classNames("flex flex-col", props.className)}>
    <div className="overflow-x-auto">
      <ol className="flex gap-2">
        <Show when={props.withHome}>
          <li className="flex whitespace-nowrap">
            <NavLink href="/" end>
              主頁
            </NavLink>
          </li>
        </Show>
        <For each={props.menu}>
          {(item) => (
            <Show when={item.actionType === "CUSTOM_URL"}>
              <li className="flex whitespace-nowrap">
                <NavLink
                  href={rewriteHK01Url(item.actionLink)}
                  referrerpolicy="same-origin"
                  end
                >
                  {item.publishName}
                </NavLink>
              </li>
            </Show>
          )}
        </For>
      </ol>
    </div>
  </nav>
);

export default Menu;
