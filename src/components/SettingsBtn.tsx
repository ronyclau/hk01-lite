import classNames from "classnames";
import { Component, For, Show, createSignal, useContext } from "solid-js";
import { Portal } from "solid-js/web";

import { SettingsContext } from "@src/contexts/SettingsContext";

export interface Props {
  className?: string;
}

const SettingsPane: Component<Props & { onClose: () => void }> = (props) => {
  const [state, { toggleSponsored, toggleCollapseAllThirdPartyBlocks }] =
    useContext(SettingsContext);

  const fields = [
    {
      label: "Hide sponsored",
      type: "toggle",
      defaultValue: state.hideSponsored,
      onInput: toggleSponsored,
    },
    {
      label: "Collapse third-party blocks",
      type: "toggle",
      defaultValue: state.collapseAllCodeBlocks,
      onInput: toggleCollapseAllThirdPartyBlocks,
    },
  ];

  return (
    <div
      className={classNames(
        "flex flex-col gap-4 p-4 rounded border-gray-50 bg-white relative",
        props.className
      )}
    >
      <button className="absolute top-2 left-2" onClick={props.onClose}>
        ✖️
      </button>
      <h2 className="text-center text-xl lg:text-2xl font-bold">Settings</h2>
      <form className="grid grid-cols-2 gap-2">
        <For each={fields}>
          {(field) => (
            <>
              <label className="justify-self-end text-right">
                {field.label}:
              </label>
              <input
                className="justify-self-start"
                type="checkbox"
                checked={field.defaultValue}
                onInput={field.onInput}
              />
            </>
          )}
        </For>
      </form>
    </div>
  );
};

const SettingsBtn: Component<Props> = (props) => {
  const [isPaneOpened, setIsPaneOpened] = createSignal(false);

  return (
    <>
      <button
        className={classNames(
          "absolute top-0 left-0 w-[22.627417px] h-[22.627417px] lg:w-[45.254834px] lg:h-[45.254834px]",
          "rounded-br-[100%] bg-gray-200",
          "flex items-start justify-start",
          props.className
        )}
        onClick={() => setIsPaneOpened(true)}
      >
        <span className="flex items-center justify-center w-[70.71067812%] h-[70.71067812%] lg:text-2xl">
          ⚙️
        </span>
      </button>
      <Show when={isPaneOpened()}>
        <Portal mount={document.head}>
          <style>{`html { height: 100vh; overflow: hidden; }`}</style>
        </Portal>
        <Portal>
          <div
            className="fixed inset-0 p-4 bg-black bg-opacity-75 flex flex-col items-stretch justify-center"
            oncapture:click={(ev: MouseEvent) => {
              if (ev.currentTarget === ev.target) {
                setIsPaneOpened(false);
              }
            }}
          >
            <SettingsPane
              className="max-h-full"
              onClose={() => setIsPaneOpened(false)}
            />
          </div>
        </Portal>
      </Show>
    </>
  );
};

export default SettingsBtn;
