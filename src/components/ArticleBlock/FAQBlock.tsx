import classNames from "classnames";
import { Component, For } from "solid-js";

import type { BlockFAQ } from "@src/types/ArticleBlock";

import Token from "./Token";

export interface Props {
  block: BlockFAQ;
  className?: string;
}

const FAQBlock: Component<Props> = (props) => (
  <div className={classNames("flex flex-col gap-2", props.className)}>
    <For each={props.block.questionsAndAnswers}>
      {(item) => (
        <details className="bg-blue-50 px-4 py-1 rounded-sm">
          <summary>
            <span className="underline">
              <Token htmlTokens={item.question.htmlTokens} inline />
            </span>
          </summary>
          <div className="ml-4 mt-2">
            <Token htmlTokens={item.answer.htmlTokens} />
          </div>
        </details>
      )}
    </For>
  </div>
);

export default FAQBlock;
