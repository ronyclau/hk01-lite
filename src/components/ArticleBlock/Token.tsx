import { Link } from "solid-app-router";
import { Component, For, Index, Match, Switch } from "solid-js";

import type { HTMLToken } from "@src/types/ArticleBlock";
import { isHK01Link, rewriteHK01Url } from "@src/utils/url";

export interface Props {
  htmlTokens: HTMLToken[][] | HTMLToken[] | HTMLToken;
  inline?: boolean;
}

const TokenContent: Component<{ content: string }> = (props) => {
  const contents = () => props.content.split("\n");
  return (
    <Index each={contents()}>
      {(content, index) =>
        index !== 0 ? (
          <>
            <br />
            {content()}
          </>
        ) : (
          content()
        )
      }
    </Index>
  );
};

const SingleToken: Component<{ token: HTMLToken }> = (props) => {
  switch (props.token.type) {
    case "text":
      return (
        <span className="empty:hidden">
          <TokenContent content={props.token.content} />
        </span>
      );
    case "boldText":
      return (
        <strong className="empty:hidden">
          <TokenContent content={props.token.content} />
        </strong>
      );
    default:
      const url = rewriteHK01Url(props.token.link);
      return (
        <Switch
          fallback={
            <a
              href={props.token.link}
              className="empty:hidden"
              referrerpolicy="same-origin"
              rel="noopener"
              target="_blank"
            >
              <TokenContent content={props.token.content} />
            </a>
          }
        >
          <Match when={isHK01Link(url)}>
            <Link href={url}>
              <TokenContent content={props.token.content} />
            </Link>
          </Match>
        </Switch>
      );
  }
};

const TokenArr: Component<{ tokens: HTMLToken[] }> = (props) => (
  <For each={props.tokens}>{(item) => <SingleToken token={item} />}</For>
);

const Token: Component<Props> = (props) => {
  return !Array.isArray(props.htmlTokens) ? (
    <SingleToken token={props.htmlTokens} />
  ) : props.htmlTokens.length > 0 && Array.isArray(props.htmlTokens[0]) ? (
    <For each={props.htmlTokens as HTMLToken[][]}>
      {(tokens) =>
        props.inline ? (
          <TokenArr tokens={tokens} />
        ) : tokens.length === 1 && tokens[0].type === "boldText" ? (
          <h3 className="empty:hidden font-bold text-lg lg:text-xl mt-4 mb-2 first:mt-0 last:mb-0">
            <TokenContent content={tokens[0].content} />
          </h3>
        ) : (
          <p className="my-2 first:mt-0 last:mb-0">
            <TokenArr tokens={tokens} />
          </p>
        )
      }
    </For>
  ) : (
    <TokenArr tokens={props.htmlTokens as HTMLToken[]} />
  );
};

export default Token;
