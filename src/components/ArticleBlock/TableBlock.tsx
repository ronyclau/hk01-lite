import type { Component } from "solid-js";

import type { BlockTable } from "@src/types/ArticleBlock";

import CodeBlock from "./CodeBlock";

export interface Props {
  block: BlockTable;
}

const TableBlock: Component<Props> = (props) => (
  <CodeBlock
    block={{ blockType: "code", htmlString: props.block.htmlString }}
  />
);

export default TableBlock;
