import classNames from "classnames";
import { Component, Show } from "solid-js";

import type { BlockQuote } from "@src/types/ArticleBlock";

export interface Props {
  block: BlockQuote;
  className?: string;
}

const QuoteBlock: Component<Props> = (props) => (
  <figure className={classNames("bg-gray-100 p-4 rounded-sm", props.className)}>
    <blockquote className="before:[content:'“'] after:[content:'”']">
      {props.block.message}
    </blockquote>
    <Show when={props.block.author}>
      <figcaption className="text-right before:[content:'—'] before:pr-[1ex]">
        {props.block.author}
      </figcaption>
    </Show>
  </figure>
);

export default QuoteBlock;
