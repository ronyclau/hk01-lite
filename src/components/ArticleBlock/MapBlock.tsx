import { computeDestinationPoint, getBounds } from "geolib";
import {
  Component,
  Show,
  createEffect,
  createMemo,
  createSignal,
} from "solid-js";

import type { BlockMap } from "@src/types/ArticleBlock";

export interface Props {
  block: BlockMap;
  className?: string;
}

const MapBlock: Component<Props> = (props) => {
  let iframeRef: HTMLIFrameElement | undefined;

  const [iframeSize, setIframeSize] = createSignal({ width: 320, height: 180 });
  createEffect(() => {
    const rect = iframeRef?.getBoundingClientRect();
    rect && setIframeSize({ height: rect.height, width: rect.width });
  });

  const mapLink = createMemo(() => {
    const url = new URL("https://www.openstreetmap.org/");
    url.searchParams.append("mlat", props.block.latitude.toFixed(5));
    url.searchParams.append("mlon", props.block.longitude.toFixed(5));
    url.hash = `map=${props.block.zoomLevel}/${props.block.latitude.toFixed(
      5
    )}/${props.block.longitude.toFixed(5)}`;
    return url.toString();
  });
  const bbox = createMemo(() => {
    const { latitude: lat, longitude: lng, zoomLevel: zoom } = props.block;
    const resolution =
      ((40075016.686 / 256) * Math.cos((lat / 180) * Math.PI)) /
      Math.pow(2, zoom);

    const { height, width } = iframeSize();

    const up = computeDestinationPoint(
      [lng, lat],
      resolution * (height / 2),
      0
    );
    const right = computeDestinationPoint(
      [lng, lat],
      resolution * (width / 2),
      90
    );
    const down = computeDestinationPoint(
      [lng, lat],
      resolution * (height / 2),
      180
    );
    const left = computeDestinationPoint(
      [lng, lat],
      resolution * (width / 2),
      270
    );
    const { minLat, maxLat, minLng, maxLng } = getBounds([
      up,
      right,
      down,
      left,
    ]);

    console.debug({
      lat,
      lng,
      zoom,
      width,
      height,
      resolution,
      up,
      right,
      down,
      left,
      minLat,
      maxLat,
      minLng,
      maxLng,
    });

    return [minLng, minLat, maxLng, maxLat].join(",");
  });
  const iframeUrl = createMemo(() => {
    const url = new URL("https://www.openstreetmap.org/export/embed.html");
    url.searchParams.append("bbox", bbox());
    url.searchParams.append("layer", "mapnik");
    url.searchParams.append(
      "marker",
      `${props.block.latitude},${props.block.longitude}`
    );
    return url.toString();
  });

  const [mapActivated, setMapActivated] = createSignal(false);

  const activateMap = (ev: MouseEvent) => {
    ev.preventDefault();
    setMapActivated(true);
  };

  return (
    <figure className={props.className}>
      <div className="flex w-full relative">
        <iframe
          ref={iframeRef}
          className="border-0 aspect-video w-full"
          src={iframeUrl()}
          style="border: 1px solid black"
          title={props.block.caption}
        ></iframe>
        <Show when={!mapActivated()}>
          <a
            className="flex items-center justify-center absolute inset-0 bg-[#ffffffa0] text-lg text-center z-10"
            href={mapLink()}
            onClick={activateMap}
          >
            Click/Tap to activate map.
          </a>
        </Show>
      </div>
      <small>
        <a
          href={mapLink()}
          rel="noreferrer noopener"
          referrerpolicy="same-origin"
          target="_blank"
        >
          View Larger Map
        </a>
      </small>
      <figcaption>{props.block.caption}</figcaption>
    </figure>
  );
};

export default MapBlock;
