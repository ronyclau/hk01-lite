import classNames from "classnames";
import { Component, For } from "solid-js";

import type { BlockSummary } from "@src/types/ArticleBlock";

export interface Props {
  block: BlockSummary;
  className?: string;
}

const SummaryBlock: Component<Props> = (props) => (
  <div
    className={classNames(
      "flex flex-col gap-2 border-l-4 border-l-blue-500 pl-1",
      props.className
    )}
  >
    <For each={props.block.summary}>{(row) => <p>{row}</p>}</For>
  </div>
);

export default SummaryBlock;
