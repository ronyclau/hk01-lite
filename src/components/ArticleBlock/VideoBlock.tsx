import { Component, Show } from "solid-js";

import type { BlockYoutube } from "@src/types/ArticleBlock";

export interface Props {
  block: BlockYoutube;
  className?: string;
}

const VideoBlock: Component<Props> = (props) => (
  <Show when={props.block.type === "youtube"}>
    <figure className={props.className}>
      <iframe
        className="aspect-video border-0 w-full"
        src={`https://www.youtube-nocookie.com/embed/${props.block.videoId}`}
      />
      <figcaption>{props.block.caption}</figcaption>
    </figure>
  </Show>
);

export default VideoBlock;
