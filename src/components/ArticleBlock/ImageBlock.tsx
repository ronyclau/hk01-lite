import { Component, Show } from "solid-js";

import type { BlockImage } from "@src/types/ArticleBlock";

import MediaImg from "../MediaImg";

export interface Props {
  block: BlockImage;
  galleryId?: string;
  className?: string;
}

const ImageBlock: Component<Props> = (props) => (
  <figure className={props.className}>
    <MediaImg
      media={props.block.image}
      galleryId={props.galleryId}
      ratio="16_9"
      alt={props.block.image.caption}
    />
    <Show when={props.block.image.caption.length > 0}>
      <figcaption className="text-gray-500 px-4 lg:px-0">
        {props.block.image.caption}
      </figcaption>
    </Show>
  </figure>
);

export default ImageBlock;
