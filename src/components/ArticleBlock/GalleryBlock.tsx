import classNames from "classnames";
import { Component, For } from "solid-js";

import type { BlockGallery } from "@src/types/ArticleBlock";

import MediaImg from "../MediaImg";

export const DEFAULT_GALLERY_ID = "default";

export interface Props {
  block: BlockGallery;
  galleryId?: string;
  className?: string;
}

const getLayoutClasses = (len: number, index: number) => {
  switch (len) {
    case 0:
    case 1:
      return "aspect-video";

    case 2:
      return "basis-1/2 w-1/2 aspect-[3/2] lg:aspect-[4/3]";

    case 3:
      return "basis-1/3 w-1/3 aspect-square lg:aspect-video";

    case 4:
      return "basis-1/2 w-1/2 aspect-[3/2] lg:basis-1/2 lg:w-1/2 lg:aspect-[8/3]";

    case 5:
      return index < 2
        ? "basis-1/2 w-1/2 aspect-[3/2] lg:aspect-[8/3]"
        : "basis-1/3 w-1/3 aspect-square lg:aspect-video";

    default:
      return "basis-1/3 w-1/3 lg:aspect-video";
  }
};

const GalleryBlock: Component<Props> = (props) => {
  return (
    <div className={classNames("flex flex-col", props.className)}>
      <div
        className="flex flex-wrap -m-px"
        style={{
          "--label-content":
            props.block.images.length > 6
              ? `"${props.block.images.length - 6}+"`
              : "",
        }}
      >
        <For each={props.block.images}>
          {(item, index) => (
            <MediaImg
              linkClass={classNames(
                "flex flex-col",
                getLayoutClasses(props.block.images.length, index()),
                "from-seventh:hidden aspect-square relative sixth:before:items-center sixth:before:justify-center sixth:before:text-4xl sixth:before:font-bold sixth:before:absolute sixth:before:flex sixth:before:inset-0 sixth:before:touch-none sixth:before:content-[var(--label-content)] sixth:before:bg-[#ffffffa0]"
              )}
              galleryId={props.galleryId}
              media={item}
              className="m-px object-cover object-center h-full"
            />
          )}
        </For>
      </div>
    </div>
  );
};

export default GalleryBlock;
