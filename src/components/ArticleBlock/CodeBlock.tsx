import base64js from "base64-js";
import classNames from "classnames";
import { Component, createMemo } from "solid-js";

import type { BlockCode } from "@src/types/ArticleBlock";

export interface Props {
  block: BlockCode;
  className?: string;
}

const CodeBlock: Component<Props> = (props) => {
  const getDataUrl = createMemo(
    (html: string) =>
      `data:text/html;charset=utf-8;base64,${base64js.fromByteArray(
        new TextEncoder().encode(
          `<!DOCTYPE html><html><head><script src="${document.location.origin}/iframeResizer.contentWindow.min.js" async></script></head><body>${html}</body></html>`
        )
      )}`,
    props.block.htmlString
  );

  return (
    <div className={classNames("flex flex-col", props.className)}>
      <iframe
        className="border-0 w-full"
        referrerpolicy="no-referrer"
        src={getDataUrl()}
        loading="lazy"
      ></iframe>
    </div>
  );
};

export default CodeBlock;
