import classNames from "classnames";
import type { Component } from "solid-js";

import type { BlockText } from "@src/types/ArticleBlock";

import Token from "./Token";

export interface Props {
  block: BlockText;
  className?: string;
}

const TextBlock: Component<Props> = (props) => (
  <div className={classNames("empty:hidden", props.className)}>
    <Token htmlTokens={props.block.htmlTokens} />
  </div>
);

export default TextBlock;
