import {
  Component,
  Show,
  createEffect,
  createResource,
  createSignal,
  onCleanup,
} from "solid-js";
import { Portal } from "solid-js/web";

import type { BlockBrightcove } from "@src/types/ArticleBlock";
import type { VideoInfo } from "@src/types/Video";
import { importScript } from "@src/utils/importScript";

export interface Props {
  block: BlockBrightcove;
  className?: string;
}

const fetchVideoInfo = (videoId: string): Promise<VideoInfo> =>
  fetch(`/hk01vdo/${videoId}/info.json`, {
    method: "GET",
  }).then((res) => res.json());

const initVdo = async (ele: HTMLVideoElement, src: string, poster?: string) => {
  if (!("videojs" in window)) {
    await importScript("/video.min.js");
  }
  if ("videojs" in window) {
    return (
      window as typeof window & {
        videojs: (...params: unknown[]) => {
          dispose: () => void;
        };
      }
    ).videojs(ele, {
      aspectRatio: "16:9",
      sources: [
        {
          src,
          type: "application/x-mpegURL",
        },
      ],
      poster,
      preload: "metadata",
    });
  }
};

const SelfHostedVideoBlock: Component<Props> = (props) => {
  let [ref, setRef] = createSignal<HTMLVideoElement | undefined>(undefined);
  const [data] = createResource(() => props.block.videoId, fetchVideoInfo);

  createEffect(() => {
    const ele = ref();
    if (ele) {
      let cleanedUp = false;
      onCleanup(() => {
        cleanedUp = true;
        console.debug("onCleanup");
      });

      initVdo(ele, data()?.url ?? "", data()?.img).then((player) => {
        if (player) {
          if (cleanedUp) {
            player.dispose();
            console.debug("player disposed");
          } else {
            onCleanup(() => {
              player.dispose();
              console.debug("player disposed");
            });
          }
        }
      });
    }
  });

  return (
    <Show when={props.block.type === "self_host_video"}>
      <Portal mount={document.head}>
        <link href="/video-js.min.css" rel="stylesheet" />
        <style>{`.video-js .vjs-big-play-button { top: 50%; left: 50%; transform: translate3d(-50%, -50%, 0); }`}</style>
      </Portal>
      <Show when={props.block}>
        {(block) => (
          <figure className={props.className}>
            <Show
              when={!data.loading && !data.error}
              fallback={<div className="bg-gray-200 aspect-video" />}
            >
              <video
                className="video-js aspect-video w-full"
                controls
                ref={setRef}
              />
            </Show>
            <figcaption>{block.caption}</figcaption>
          </figure>
        )}
      </Show>
    </Show>
  );
};

export default SelfHostedVideoBlock;
