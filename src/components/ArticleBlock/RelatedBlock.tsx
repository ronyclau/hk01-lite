import classNames from "classnames";
import { Link } from "solid-app-router";
import { Component, For, Match, Show, Switch, useContext } from "solid-js";

import { SettingsContext } from "@src/contexts/SettingsContext";
import type { BlockRelated } from "@src/types/ArticleBlock";
import type { ArticleFeedItem, IssueFeedItem } from "@src/types/FeedItem";

export interface Props {
  block: BlockRelated;
  className?: string;
}

const RelatedBlock: Component<Props> = (props) => {
  const [state] = useContext(SettingsContext);

  return (
    <ul className={classNames("flex flex-col items-start", props.className)}>
      <For each={props.block.articles}>
        {(article) => (
          <Show
            when={
              !(article as ArticleFeedItem).data.isSponsored ||
              !state.hideSponsored
            }
          >
            <li className="flex">
              <Show
                when={
                  article.type === 1 &&
                  (article as ArticleFeedItem).data.isSponsored
                }
              >
                <span
                  className="bg-black text-center inline-block w-7 lg:w-10 mr-1"
                  title="Sponsored"
                >
                  💰
                </span>
              </Show>
              <Switch>
                <Match when={article.type === 1}>
                  <Link href={`/article/${article.id}`}>
                    {(article as ArticleFeedItem).data.title}
                  </Link>
                </Match>
                <Match when={article.type === 2}>
                  <Link href={`/issue/${article.id}`}>
                    {(article as IssueFeedItem).data.title}
                  </Link>
                </Match>
              </Switch>
            </li>
          </Show>
        )}
      </For>
    </ul>
  );
};

export default RelatedBlock;
