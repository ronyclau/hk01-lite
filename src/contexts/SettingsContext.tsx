import { Component, createContext, createEffect, splitProps } from "solid-js";
import { createStore } from "solid-js/store";

import type { Settings } from "@src/types/Settings";
import { pick } from "@src/utils/pick";

const noop = () => {};

const defaultSettings: Settings = {
  hideSponsored: true,
  collapseAllCodeBlocks: true,
};

export const SettingsContext = createContext<[Settings, SettingsActions]>([
  defaultSettings,
  new Proxy({} as SettingsActions, {
    get: () => noop,
  }),
]);

export interface Props extends Partial<Settings> {}

export interface SettingsActions {
  toggleSponsored: () => void;
  setHideSponsored: (shouldHide: boolean) => void;
  toggleCollapseAllThirdPartyBlocks: () => void;
  setCollapseAllThirdPartyBlocks: (shouldCollapse: boolean) => void;
}

export const SettingsProvider: Component<Props> = (props) => {
  const [local, settings] = splitProps(props, ["children"]);
  const [state, setState] = createStore<Settings>({
    ...defaultSettings,
    ...settings,
  });
  const storeActions: SettingsActions = {
    toggleSponsored() {
      setState("hideSponsored", (v) => !v);
    },
    setHideSponsored(shouldHide: boolean) {
      setState("hideSponsored", shouldHide);
    },
    toggleCollapseAllThirdPartyBlocks() {
      setState("collapseAllCodeBlocks", (v) => !v);
    },
    setCollapseAllThirdPartyBlocks(shouldCollapse: boolean) {
      setState("collapseAllCodeBlocks", shouldCollapse);
    },
  };

  createEffect(() => {
    console.log("Loading settings from localStorage...");
    try {
      const rawStoredSettings = window.localStorage.getItem("settings");
      if (rawStoredSettings === null) {
        console.log("Loading settings from localStorage... Done, none stored.");
      } else {
        const storedSettings = {
          ...defaultSettings,
          ...pick(JSON.parse(rawStoredSettings), Object.keys(defaultSettings)),
        };
        setState(storedSettings);
        console.log(
          "Loading settings from localStorage... Done:",
          storedSettings
        );
      }
    } catch (err) {
      console.error("Loading settings from localStorage... Failed:", err);
    }
  });

  createEffect(() => {
    try {
      console.log("persisting settings...");
      window.localStorage.setItem(
        "settings",
        JSON.stringify(pick({ ...state }, Object.keys(defaultSettings)))
      );
    } catch {}
  });

  return (
    <SettingsContext.Provider value={[state, storeActions]}>
      {local.children}
    </SettingsContext.Provider>
  );
};
