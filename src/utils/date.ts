import { format, formatRelative, intervalToDuration } from "date-fns";
import { zhHK as locale } from "date-fns/locale";

export const relativeDate: typeof formatRelative = (
  date,
  baseDate,
  options
) => {
  const {
    days = 0,
    weeks = 0,
    months = 0,
    years = 0,
  } = intervalToDuration({
    start: baseDate,
    end: date,
  });

  return days >= 7 || weeks > 0 || months > 0 || years > 0
    ? format(date, "yyyy-MM-dd", { locale })
    : formatRelative(date, baseDate, { locale, ...options });
};

export const formatDate: typeof format = (date, formatStr, options) =>
  format(date, formatStr, { locale, ...options });
