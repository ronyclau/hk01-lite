import { LRUMap } from "lru_map";

import config from "@src/config";
import type { Article } from "@src/types/Article";
import type { Feed } from "@src/types/Feed";
import type { FeedItem } from "@src/types/FeedItem";
import type { Issue } from "@src/types/Issue";
import type { Menu as MenuType } from "@src/types/Menu";
import type {
  PageCategory,
  PageHome,
  PageListing,
  PageZone,
} from "@src/types/Pages";
import type { TagInfo } from "@src/types/Tag";

const cache = new LRUMap(30);

export const withOffset = (url: string, offset?: number | string | null) => {
  if (offset !== undefined && offset !== null) {
    const urlObj = new URL(url, config.apiBaseUrl);
    urlObj.searchParams.set("offset", `${offset}`);
    return urlObj.toString();
  } else {
    return url;
  }
};

export const genericFetcher = (url: string): Promise<Response> =>
  fetch(new URL(url, config.apiBaseUrl).toString());

export const genericJsonFetcher = async (url: string): Promise<any> => {
  const cachedData = cache.get(url);
  if (cachedData !== undefined) {
    return cachedData;
  } else {
    const res = await genericFetcher(url);
    const data = await res.json();
    cache.set(url, data);
    return data;
  }
};

export const fetchHome = (): Promise<PageHome> =>
  genericJsonFetcher("v2/page/home");

export const fetchHomeMenu = (): Promise<MenuType> =>
  genericJsonFetcher("v2/menus/321").then((obj) => obj.items ?? []);

export const fetchArticle = (articleId: number | string): Promise<Article> =>
  genericJsonFetcher(`v2/page/article/${articleId}`).then(
    ({ article }) => article
  );

export const fetchIssue = (issueId: number | string): Promise<Issue> =>
  genericJsonFetcher(`v2/page/issue/${issueId}`).then(({ issue }) => issue);

export const fetchCategory = (
  categoryId: number | string
): Promise<PageCategory> =>
  genericJsonFetcher(`v2/page/category/${categoryId}`);

export const fetchZone = (zoneId: number | string): Promise<PageZone> =>
  genericJsonFetcher(`v2/page/zone/${zoneId}`);

export const fetchRelatedArticles = (
  articleId: number | string
): Promise<{ items: FeedItem[] }> =>
  genericJsonFetcher(`v2/articles/${articleId}/related`);

export const fetchHotPage = (): Promise<PageListing> =>
  genericJsonFetcher(`v2/page/hot`);

export const fetchHot = (offset?: number | string): Promise<Feed> =>
  genericJsonFetcher(withOffset(`v2/feed/hot`, offset));

export const fetchLatestPage = (): Promise<PageListing> =>
  genericJsonFetcher(`v2/page/latest`);

export const fetchLatest = (offset?: number | string): Promise<Feed> =>
  genericJsonFetcher(withOffset(`v2/feed/category/0`, offset));

export const fetchTagMeta = (tagId: number | string): Promise<TagInfo> =>
  genericJsonFetcher(`v2/tags/${tagId}`);

export const fetchTagArticles = (
  tagId: number | string,
  offset?: number | string
): Promise<{
  items: FeedItem[];
  nextOffset?: number | string;
}> => genericJsonFetcher(withOffset(`v2/feed/tag/${tagId}`, offset));
