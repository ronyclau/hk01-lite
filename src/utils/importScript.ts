const scriptsImported = new Map<string, Promise<HTMLScriptElement>>();

export const importScript = (
  src: string,
  forced = false
): Promise<HTMLScriptElement> => {
  const promise = scriptsImported.get(src);
  if (!forced && promise !== undefined) {
    return promise;
  } else {
    const newPromise = new Promise<HTMLScriptElement>((resolve, reject) => {
      const ele = document.createElement("script");
      ele.type = "text/javascript";
      ele.src = src;
      ele.addEventListener("load", () => {
        resolve(ele);
      });
      ele.addEventListener("error", (err) => {
        reject(err.error);
      });
      document.body.appendChild(ele);
    });
    scriptsImported.set(src, newPromise);
    return newPromise;
  }
};
