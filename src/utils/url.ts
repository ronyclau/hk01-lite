const hk01DomainRegex =
  /^https?:\/\/(([^.]+\.)?hk(01|tester)\.com|hkdev\.com:[1-9][0-9]+)$/;

const integerRegex = /^\d+$/;

export const rewriteHK01Url = (url: string): string => {
  try {
    const urlObj = new URL(url);
    if (hk01DomainRegex.test(urlObj.origin)) {
      const pathParts = (
        urlObj.pathname.endsWith("/")
          ? urlObj.pathname.slice(0, -1)
          : urlObj.pathname
      ).split("/");
      if (pathParts.length <= 1) {
        return urlObj.hostname.startsWith("mensluxe.") ? "/zone/20" : "/";
      } else if (pathParts.length === 2) {
        if (pathParts[1] === "hot") {
          return "/hot";
        } else if (pathParts[1] === "latest") {
          return "/latest";
        } else {
          return url;
        }
      } else if (pathParts[1] === "tag") {
        return `/tag/${pathParts[2]}`;
      } else if (pathParts[1] === "issue") {
        return `/issue/${pathParts[2]}`;
      } else if (["a", "article"].includes(pathParts[1])) {
        return `/article/${pathParts[2]}`;
      } else if (pathParts[1] === "zone") {
        return `/zone/${pathParts[2]}`;
      } else if (pathParts[1] === "channel") {
        return `/category/${pathParts[2]}`;
      } else if (pathParts.length === 4 && integerRegex.test(pathParts[2])) {
        return `/article/${pathParts[2]}`;
      }
    }
    return url;
  } catch {
    return url;
  }
};

export const isHK01Link = (url: string) => {
  if (url.startsWith("/") || url.startsWith("./")) {
    return true;
  }

  try {
    const urlObj = new URL(url, document.location.href);
    return hk01DomainRegex.test(urlObj.origin);
  } catch {
    return false;
  }
};
