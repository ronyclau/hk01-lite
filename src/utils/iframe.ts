import { IFrameComponent, iframeResizer } from "iframe-resizer";
import type { JSX } from "solid-js";

export const resizeIframeOnDetailsToggle: JSX.EventHandler<
  HTMLDetailsElement,
  Event
> = (ev) => {
  if (ev.currentTarget.open) {
    console.debug("toggled open", ev.currentTarget);
    const iframe = ev.currentTarget.querySelector("iframe");
    if (iframe) {
      resizeIframe(iframe);
    }
  }
};

export const resizeIframe = (iframe: HTMLIFrameElement) => {
  if ("iFrameResizer" in iframe) {
    console.debug("toggled open, resizing", iframe);
    (iframe as IFrameComponent).iFrameResizer.resize();
  } else {
    console.debug("toggled open, resizer not inited, init now", iframe);
    iframeResizer({ checkOrigin: false }, iframe);
  }
};
