export const pick = <T extends object, K extends (string | number | symbol)[]>(
  obj: T,
  keys: K
): Partial<T> => {
  const newObj: Record<string | number | symbol, unknown> = {};
  for (const key of keys) {
    if (key in obj) {
      newObj[key] = (obj as any)[key];
    }
  }
  return newObj as Partial<T>;
};
