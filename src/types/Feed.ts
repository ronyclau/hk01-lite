import type { FeedItem } from "./FeedItem";

export interface Feed {
  items: FeedItem[];
  nextOffset?: number | string;
}
