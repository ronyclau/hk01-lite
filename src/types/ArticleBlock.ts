import type { Media } from "./Article";
import type { FeedItem } from "./FeedItem";

interface HTMLTokenText {
  type: "text";
  content: string;
}

interface HTMLTokenBoldText {
  type: "boldText";
  content: string;
}

interface HTMLTokenLink {
  type: "link";
  content: string;
  link: string;
}

export type HTMLToken = HTMLTokenText | HTMLTokenBoldText | HTMLTokenLink;

export interface BlockImage {
  blockType: "image" | "imgtxt" | "txtimg";
  htmlTokens: HTMLToken[];
  image: Media;
}

export interface BlockGallery {
  blockType: "gallery";
  images: Media[];
}

// interface BlockMotiongraphic {
//   blockType: "motiongraphic";
// }

export interface BlockText {
  blockType: "text";
  htmlTokens: HTMLToken[];
}

export interface BlockSummary {
  blockType: "summary";
  summary: string[];
}

export interface BlockQuote {
  blockType: "quote";
  message: string;
  author: string;
}

export interface BlockTable {
  blockType: "table";
  htmlString: string;
}

export interface BlockRelated {
  blockType: "related";
  title?: string;
  articles: FeedItem[];
}

export interface BlockFeatured extends Omit<BlockRelated, "blockType"> {
  blockType: "featured";
}

export interface BlockMap {
  blockType: "map";
  latitude: number;
  longitude: number;
  zoomLevel: number;
  caption: string;
}

export interface BlockCode {
  blockType: "code";
  image?: Media;
  htmlString: string;
}

export interface BlockYoutube {
  blockType: "video";
  videoId: string;
  duration: number;
  type: "youtube";
  thumbnails: {
    defaultQuality?: string;
    highQuality?: string;
    mediumQuality?: string;
    standQuality?: string;
    maxResolution?: string;
  };
  caption: string;
}

export interface BlockBrightcove {
  blockType: "brightcove";
  videoId: string;
  duration: number;
  type: "self_host_video";
  thumbnails: {
    defaultQuality?: string;
  };
  caption: string;
  videoType: string;
  mediaVideoId: number;
  playerId: null;
  height: number;
  width: number;
}

export type BlockVideo = BlockYoutube | BlockBrightcove;

// interface BlockAudio {
//   blockType: "audio";
// }

// interface BlockMusic {
//   blockType: "music";
// }

// interface BlockVcp {
//   blockType: "vcp";
// }

export interface BlockFAQ {
  blockType: "faq";
  questionsAndAnswers: {
    question: {
      htmlTokens: HTMLToken[][];
    };
    answer: {
      htmlTokens: HTMLToken[][];
    };
  }[];
}

export type Block =
  | BlockImage
  | BlockGallery
  | BlockText
  | BlockSummary
  | BlockQuote
  | BlockTable
  | BlockRelated
  | BlockFeatured
  | BlockMap
  | BlockCode
  | BlockYoutube
  | BlockBrightcove
  | BlockFAQ;
