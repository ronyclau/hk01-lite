export interface TagInfo {
  tagId: number;
  canonicalUrl: string;
  publishUrl: string;
  tagName: string;
  isSponsored: number | boolean;
}
