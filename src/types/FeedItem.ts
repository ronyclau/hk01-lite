import type { ListArticle } from "./Article";
import type { ListIssue } from "./Issue";

export interface ArticleFeedItem {
  id: number | string;
  type: 1;
  data: ListArticle;
}

export interface IssueFeedItem {
  id: number | string;
  type: 2;
  data: ListIssue;
}

export interface GenericFeedItem {
  id: number | string;
  type: Exclude<number, 1 | 2>;
  data: object;
}

export type FeedItem = ArticleFeedItem | IssueFeedItem | GenericFeedItem;
