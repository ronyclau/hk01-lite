export interface Settings {
  hideSponsored: boolean;
  collapseAllCodeBlocks: boolean;
}
