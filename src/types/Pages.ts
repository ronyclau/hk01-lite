import type { FeedItem } from "./FeedItem";
import type { Menu } from "./Menu";

export interface PageSection extends PageListing {
  name: string;
  type: string;
  id: number;
  moreUrl?: string;
  maxItems?: number;
  nextPageUrl?: string;
}

export interface PageZoneInfo {
  zoneId: number;
  canonicalUrl: string;
  publishName: string;
  publishUrl: string;
  deepLink: string;
  icon: string;
}

export interface PageCategoryInfo {
  categoryId: number;
  publishName: string;
  publishUrl: string;
  deepLink: string;
  icon: string;
}

export interface PageData {
  menu: Menu;
}

export interface PageWithSections extends PageData {
  sections: PageSection[];
}

export interface PageZone extends PageWithSections {
  zone: PageZoneInfo;
}

export interface PageCategory extends PageZone {
  category: PageCategoryInfo;
}

export interface PageHome extends Omit<PageWithSections, "menu"> {}

export interface PageListing {
  items: FeedItem[];
  nextOffset?: string | number;
}
