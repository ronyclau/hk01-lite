export interface VideoInfo {
  img: string;
  title: string;
  description: string;
  url: string;
}
