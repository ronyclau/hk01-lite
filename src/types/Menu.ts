export interface MenuItem {
  id: number;
  publishName: string;
  actionType: string;
  actionLink: string;
  desc: string;
  channelIcon: string | null;
  children: MenuItem[];
}

export type Menu = MenuItem[];
