import type { Media } from "./Article";
import type { Block } from "./ArticleBlock";

export interface PageIssue {
  zone: {
    zoneId: number;
    canonicalUrl: string;
    publishUrl: string;
    deepLink: string;
    name: string;
    publishName: string;
    template: string;
    colorSchema: string;
    icon: string;
  };
  issue: Issue;
}

export interface ListIssue {
  issueId: number;
  canonicalUrl: string;
  publishUrl: string;
  title: string;
  issueType: {
    publishName: string;
  };
  zoneId: number;
  zonePublishName: string;
  zoneIcon: string;
  teaser: string[];
  mainImage: Media;
  originalImage: Media;
  isFeatured: number | boolean;
  isSponsored: number | boolean;
  publishTime: number;
  lastModifyTime: number;
  articleCount: number;
  imageCount: number;
  videoCount: number;
}

export interface Issue extends ListIssue {
  blocks: Block[];
}
