import type { Block, BlockVideo } from "./ArticleBlock";
import type { TagInfo } from "./Tag";

export interface Media {
  mediaId: number;
  originalWidth: number;
  originalHeight: number;
  caption: string;
  cdnUrl: string;
}

export interface PageArticle {
  zone: {
    zoneId: number;
    canonicalUrl: string;
    publishUrl: string;
    deepLink: string;
    name: string;
    publishName: string;
    template: string;
    colorSchema: string;
    icon: string;
  };
  category: {
    categoryId: number;
    publishName: string;
    publishUrl: string;
    deepLink: string;
    template: string;
    colorSchema: string;
    icon: string;
  };
  article: Article;
}

export interface ListArticle {
  articleId: number;
  canonicalUrl: string;
  publishUrl: string;
  redirectUrl: string | null;
  title: string;
  metaTitle: string;
  authors: {
    publishName: string;
  }[];
  mainCategoryId: number;
  mainCategory: string;
  mainCategoryIcon: string | null;
  categories: {
    categoryId: number;
    publishName: string;
    publishUrl: string;
    deepLink: string;
    icon: string;
  }[];
  isFeatured: boolean | number;
  isSponsored: boolean | number;
  contentType: string;
  type: string;
  description: string;
  imageCount: number;
  videoCount: number;
  video?: BlockVideo;
  mainImage: Media;
  originalImage: Media;
  thumbnails: Media[];
  gallery: Media[];
  blockReaction: boolean;
  blockComment: boolean;
  socialReactions: {
    reactionId: string;
    totalCount: number;
  }[];
  commentCount: number;
  comment: {
    url: string;
    categoryId: number;
  };
  repostCount: number;
  publishTime: number;
  lastModifyTime: number;
  tags: TagInfo[];
  ad: {
    footnote: true;
  };
}

export interface Article extends ListArticle {
  teaser: string[];
  blocks: Block[];
}
