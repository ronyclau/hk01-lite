import { useData } from "solid-app-router";
import { Component, Show, Suspense } from "solid-js";

import LoadingPlaceholder from "@src/components/LoadingPlaceholder";
import PageWrapper from "@src/components/PageWrapper";
import ZoneListing from "@src/components/ZoneListing";

import type { PageDataType } from "./data";

const ZonePage: Component = () => {
  const data = useData<PageDataType>();

  return (
    <PageWrapper>
      <Suspense fallback={<LoadingPlaceholder />}>
        <Show when={data()}>
          <ZoneListing
            pageZone={data() as Exclude<ReturnType<typeof data>, undefined>}
          />
        </Show>
      </Suspense>
    </PageWrapper>
  );
};

export default ZonePage;
