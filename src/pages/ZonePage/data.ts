import type { RouteDataFuncArgs } from "solid-app-router";
import { Resource, createResource } from "solid-js";

import { fetchZone } from "@src/utils/fetchers";

export type PageDataType = Resource<
  Awaited<ReturnType<typeof fetchZone>> | undefined
>;

export default function ZonePageData({
  params,
}: RouteDataFuncArgs): PageDataType {
  const [data] = createResource(() => params.id, fetchZone);
  return data;
}
