import { useData } from "solid-app-router";
import { Component, Show, Suspense } from "solid-js";

import ArticleDetails from "@src/components/ArticleDetails";
import LoadingPlaceholder from "@src/components/LoadingPlaceholder";
import PageWrapper from "@src/components/PageWrapper";

import type { PageDataType } from "./data";

const ArticlePage: Component = () => {
  const data = useData<PageDataType>();

  return (
    <PageWrapper>
      <Suspense fallback={<LoadingPlaceholder />}>
        <Show when={data()}>
          <ArticleDetails
            article={data() as Exclude<ReturnType<typeof data>, undefined>}
          />
        </Show>
      </Suspense>
    </PageWrapper>
  );
};

export default ArticlePage;
