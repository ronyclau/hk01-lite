import type { RouteDataFuncArgs } from "solid-app-router";
import { Resource, createResource } from "solid-js";

import { fetchArticle } from "@src/utils/fetchers";

export type PageDataType = Resource<
  Awaited<ReturnType<typeof fetchArticle>> | undefined
>;

export default function ArticlePageData({
  params,
}: RouteDataFuncArgs): PageDataType {
  const [data] = createResource(() => params.id, fetchArticle);
  return data;
}
