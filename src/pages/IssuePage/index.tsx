import { useData } from "solid-app-router";
import { Component, Show, Suspense } from "solid-js";

import IssueDetails from "@src/components/IssueDetails";
import LoadingPlaceholder from "@src/components/LoadingPlaceholder";
import PageWrapper from "@src/components/PageWrapper";

import type { PageDataType } from "./data";

const IssuePage: Component = () => {
  const data = useData<PageDataType>();

  return (
    <PageWrapper>
      <Suspense fallback={<LoadingPlaceholder />}>
        <Show when={data()}>
          <IssueDetails
            issue={data() as Exclude<ReturnType<typeof data>, undefined>}
          />
        </Show>
      </Suspense>
    </PageWrapper>
  );
};

export default IssuePage;
