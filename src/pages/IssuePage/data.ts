import type { RouteDataFuncArgs } from "solid-app-router";
import { Resource, createResource } from "solid-js";

import { fetchIssue } from "@src/utils/fetchers";

export type PageDataType = Resource<
  Awaited<ReturnType<typeof fetchIssue>> | undefined
>;

export default function IssuePageData({
  params,
}: RouteDataFuncArgs): PageDataType {
  const [data] = createResource(() => params.id, fetchIssue);
  return data;
}
