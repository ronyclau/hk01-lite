import { useLocation, useNavigate } from "solid-app-router";
import type { Component } from "solid-js";

const pageAliases = {
  a: "article",
  c: "category",
  z: "zone",
  i: "issue",
  t: "tag",
} as const;

const pagePrefixes = new Set(Object.values(pageAliases));

const RedirectPage: Component = () => {
  const navigate = useNavigate(),
    location = useLocation();

  const pathParts = location.pathname.split("/");
  pathParts.shift();

  let found = false;

  if (pathParts.length === 2) {
    const type = pathParts[0];
    if (type in pageAliases) {
      found = true;
      pathParts[0] = pageAliases[type as keyof typeof pageAliases];
      navigate("/" + pathParts.join("/"), { replace: true });
    }
  }

  if (
    !found &&
    pathParts.length >= 2 &&
    pagePrefixes.has(pathParts[0] as any)
  ) {
    found = true;
    navigate("/" + pathParts.slice(0, 2).join("/"), { replace: true });
  }

  if (!found && pathParts.length >= 2 && /\d+/.test(pathParts[1])) {
    found = true;
    navigate(`/article/${pathParts[1]}`, { replace: true });
  }

  return null;
};

export default RedirectPage;
