import { useData, useParams } from "solid-app-router";
import { Component, Suspense, createResource } from "solid-js";

import LazyLoadingListing from "@src/components/LazyLoadingListing";
import LoadingPlaceholder from "@src/components/LoadingPlaceholder";
import PageWrapper from "@src/components/PageWrapper";
import { fetchTagArticles, fetchTagMeta } from "@src/utils/fetchers";

import type { PageDataType } from "./data";

const TagPage: Component = () => {
  const params = useParams();
  const [tagMeta] = createResource(() => params.id, fetchTagMeta);
  const data = useData<PageDataType>();

  return (
    <PageWrapper>
      <Suspense fallback={<h1 className="text-3xl font-bold">...</h1>}>
        <h1 className="text-3xl font-bold">{tagMeta()?.tagName}</h1>
      </Suspense>
      <Suspense fallback={<LoadingPlaceholder />}>
        <LazyLoadingListing
          data={data()}
          fetcher={(nextOffset?: number | string) =>
            fetchTagArticles(params.id, nextOffset)
          }
        />
      </Suspense>
    </PageWrapper>
  );
};

export default TagPage;
