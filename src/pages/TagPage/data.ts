import type { RouteDataFuncArgs } from "solid-app-router";
import { Resource, createResource } from "solid-js";

import { fetchTagArticles } from "@src/utils/fetchers";

export type PageDataType = Resource<
  Awaited<ReturnType<typeof fetchTagArticles>> | undefined
>;

export default function TagPageData({
  params,
}: RouteDataFuncArgs): PageDataType {
  const [data] = createResource(
    () => params.id,
    (id) => fetchTagArticles(id)
  );
  return data;
}
