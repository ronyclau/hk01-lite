import { Resource, createResource } from "solid-js";

import { fetchHotPage } from "@src/utils/fetchers";

export type PageDataType = Resource<
  Awaited<ReturnType<typeof fetchHotPage>> | undefined
>;

export default function HotPageData(): PageDataType {
  const [data] = createResource(fetchHotPage);
  return data;
}
