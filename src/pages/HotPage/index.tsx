import { useData } from "solid-app-router";
import { Component, Show, Suspense } from "solid-js";

import LazyLoadingListing from "@src/components/LazyLoadingListing";
import LoadingPlaceholder from "@src/components/LoadingPlaceholder";
import PageWrapper from "@src/components/PageWrapper";
import { fetchHot } from "@src/utils/fetchers";

import type { PageDataType } from "./data";

const HotPage: Component = () => {
  const data = useData<PageDataType>();

  return (
    <PageWrapper>
      <Suspense fallback={<LoadingPlaceholder />}>
        <Show when={data()}>
          <h1 className="text-3xl font-bold">Hot</h1>
          <LazyLoadingListing data={data()} fetcher={fetchHot} />
        </Show>
      </Suspense>
    </PageWrapper>
  );
};

export default HotPage;
