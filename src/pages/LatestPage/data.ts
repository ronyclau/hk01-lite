import { Resource, createResource } from "solid-js";

import { fetchLatestPage } from "@src/utils/fetchers";

export type PageDataType = Resource<
  Awaited<ReturnType<typeof fetchLatestPage>> | undefined
>;

export default function LatestPageData(): PageDataType {
  const [data] = createResource(fetchLatestPage);
  return data;
}
