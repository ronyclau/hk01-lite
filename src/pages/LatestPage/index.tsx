import { useData } from "solid-app-router";
import { Component, Show, Suspense } from "solid-js";

import LazyLoadingListing from "@src/components/LazyLoadingListing";
import LoadingPlaceholder from "@src/components/LoadingPlaceholder";
import PageWrapper from "@src/components/PageWrapper";
import { fetchLatest } from "@src/utils/fetchers";

import type { PageDataType } from "./data";

const LatestPage: Component = () => {
  const data = useData<PageDataType>();

  return (
    <PageWrapper>
      <Suspense fallback={<LoadingPlaceholder />}>
        <Show when={data()}>
          <h1 className="text-3xl font-bold">Latest</h1>
          <LazyLoadingListing data={data()} fetcher={fetchLatest} />
        </Show>
      </Suspense>
    </PageWrapper>
  );
};

export default LatestPage;
