import { Component, Match, Switch, createEffect } from "solid-js";

import PageWrapper from "@src/components/PageWrapper";

export interface Props {
  error: unknown;
}

const ErrorPage: Component<Props> = ({ error }) => {
  createEffect(() => {
    console.error("Failed to render page: ", error);
  });

  return (
    <PageWrapper>
      <h1>Failed to render page.</h1>
      <details>
        <Switch fallback={`${error}`}>
          <Match when={error instanceof Error}>
            {(error as Error).name}: {(error as Error).message}
          </Match>
          <Match when={typeof error === "string"}>{error as string}</Match>
        </Switch>
      </details>
    </PageWrapper>
  );
};

export default ErrorPage;
