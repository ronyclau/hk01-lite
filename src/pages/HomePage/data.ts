import { Resource, createResource } from "solid-js";

import { fetchHome } from "@src/utils/fetchers";

export type PageDataType = Resource<
  Awaited<ReturnType<typeof fetchHome>> | undefined
>;

export default function HomePageData(): PageDataType {
  const [data] = createResource(fetchHome);
  return data;
}
