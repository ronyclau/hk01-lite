import { useData } from "solid-app-router";
import { Component, Show, Suspense } from "solid-js";

import LoadingPlaceholder from "@src/components/LoadingPlaceholder";
import PageWrapper from "@src/components/PageWrapper";
import Sections from "@src/components/Sections";

import type { PageDataType } from "./data";

const HomePage: Component = () => {
  const data = useData<PageDataType>();

  return (
    <PageWrapper>
      <Suspense fallback={<LoadingPlaceholder />}>
        <Show when={data()}>
          <Sections sections={data()?.sections ?? []} />
        </Show>
      </Suspense>
    </PageWrapper>
  );
};

export default HomePage;
