import type { RouteDataFuncArgs } from "solid-app-router";
import { Resource, createResource } from "solid-js";

import { fetchCategory } from "@src/utils/fetchers";

export type PageDataType = Resource<
  Awaited<ReturnType<typeof fetchCategory>> | undefined
>;

export default function CategoryPageData({
  params,
}: RouteDataFuncArgs): PageDataType {
  const [data] = createResource(() => params.id, fetchCategory);
  return data;
}
