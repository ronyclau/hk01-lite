import { useData } from "solid-app-router";
import { Component, Show, Suspense } from "solid-js";

import CategoryListing from "@src/components/CategoryListing";
import LoadingPlaceholder from "@src/components/LoadingPlaceholder";
import PageWrapper from "@src/components/PageWrapper";

import type { PageDataType } from "./data";

const CategoryPage: Component = () => {
  const data = useData<PageDataType>();

  return (
    <PageWrapper>
      <Suspense fallback={<LoadingPlaceholder />}>
        <Show when={data()}>
          <CategoryListing
            pageCategory={data() as Exclude<ReturnType<typeof data>, undefined>}
          />
        </Show>
      </Suspense>
    </PageWrapper>
  );
};

export default CategoryPage;
