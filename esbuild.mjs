import path from "path";

import postCssPlugin from "@deanc/esbuild-plugin-postcss";
import { build, serve } from "esbuild";
import { solidPlugin } from "esbuild-plugin-solid";
import fsExtra from "fs-extra";

import postcssConfig from "./postcss.config.js";

const PORT =
  process.env.PORT === undefined ? 3000 : parseInt(process.env.PORT, 10);

/** @type import("esbuild").CommonOptions */
const buildOptions = {
  entryPoints: ["src/index.tsx"],
  bundle: true,
  outdir: "./dist",
  format: "esm",
  splitting: true,
  minify: !process.env.SERVE,
  watch: !!process.env.WATCH,
  plugins: [solidPlugin(), postCssPlugin(postcssConfig)],
  sourcemap: process.env.WATCH || process.env.SERVE ? "external" : false,
};

// Clean-up first
(async () => {
  await fsExtra.rm("./dist", { recursive: true });

  const resCopyPromise = fsExtra
    .ensureDir("./dist")
    .then(() => fsExtra.copy("./static/", "./dist/", { dereference: true }));

  const esbuildPromise = process.env.SERVE
    ? serve({ port: PORT, servedir: "dist" }, buildOptions).then(() => {
        console.log(`Serving on port ${PORT}`);
        return;
      })
    : Promise.all([
        build(buildOptions),
        build({
          ...buildOptions,
          outdir: undefined,
          outfile: path.join(buildOptions.outdir, "index.iife.js"),
          format: "iife",
          splitting: false,
          plugins: [solidPlugin()],
        }).then(() =>
          fsExtra.rm(path.join(buildOptions.outdir, "index.iife.css"))
        ),
      ]);

  await Promise.all([resCopyPromise, esbuildPromise]);
})().catch((err) => {
  process.stderr.write(`${err}`);
  process.stderr.write("\n");
  process.exit(1);
});
