const plugin = require("tailwindcss/plugin");

module.exports = {
  content: ["static/**/*.html", "./src/**/*.tsx"],
  theme: {
    extend: {},
  },
  plugins: [
    plugin(({ addVariant }) => {
      addVariant("second", "&:nth-child(2)");
      addVariant("third", "&:nth-child(3)");
      addVariant("forth", "&:nth-child(4)");
      addVariant("fifth", "&:nth-child(5)");
      addVariant("sixth", "&:nth-child(6)");
      addVariant("seventh", "&:nth-child(7)");
      addVariant("eighth", "&:nth-child(8)");
      addVariant("ninth", "&:nth-child(9)");
      addVariant("tenth", "&:nth-child(10)");

      addVariant("from-second", "&:nth-child(n + 2)");
      addVariant("from-third", "&:nth-child(n + 3)");
      addVariant("from-forth", "&:nth-child(n + 4)");
      addVariant("from-fifth", "&:nth-child(n + 5)");
      addVariant("from-sixth", "&:nth-child(n + 6)");
      addVariant("from-seventh", "&:nth-child(n + 7)");
      addVariant("from-eighth", "&:nth-child(n + 8)");
      addVariant("from-ninth", "&:nth-child(n + 9)");
      addVariant("from-tenth", "&:nth-child(n + 10)");
    }),
  ],
};
